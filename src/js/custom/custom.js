
/*----------------------------------------
 SCROLL MENU
 ----------------------------------------*/

$(window).scroll(function () {
	if ($(this).scrollTop() > 800) {
		$("header").addClass("glide");
	} else {
		$("header").removeClass("glide");
	}
});

// $(document).ready(function () {
// 	var $menu = $("#menu");
// 	$(window).scroll(function () {
// 		if ($(this).scrollTop() > 900 && $menu.hasClass("default")) {
// 			$menu.removeClass("default").addClass("fixed");
// 		} else if ($(this).scrollTop() <= 900 && $menu.hasClass("fixed")) {
// 			$menu.removeClass("fixed").addClass("default");
// 		}
// 	});//scroll
// });


/*----------------------------------------
 Language Select
 ----------------------------------------*/


$('.language-select').click(function () {
	$(this).toggleClass('open');
});

$('.language-select li').click(function () {
	var setLang = $('.language-select').data('location'),
		dataLangSelect = $(this).data('lang')
	$('.language-select').data('location', dataLangSelect);
	$('.language-select li').removeClass('active');
	$(this).toggleClass('active');
});


/*----------------------------------------
SLICK SERVICES
 ----------------------------------------*/


$('.services-slider').slick({
	infinite: true,
	slidesToShow: 3,
	slidesToScroll: 3,
	dots: true,
	variableWidth: true,
	responsive: [
		{
			breakpoint: 1000,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				centerMode: true,
				centerPadding: '60px',
				adaptiveHeight: true,
			}
		},
		{
			breakpoint: 639,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,	
				centerMode: true			
			}
		},
	]
});


/*----------------------------------------
 SLICK CASES
 ----------------------------------------*/


$('.cases-slider').slick({
	infinite: true,
	dots: true
});


/*----------------------------------------
 SLICK TEAM
 ----------------------------------------*/


$('.team-slider').slick({
	infinite: true,
	dots: true,
	slidesToShow: 3,
	slidesToScroll: 3,
	responsive: [
		{
			breakpoint: 980,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		},
		{
			breakpoint: 639,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				customPaging : function(slider, i) {
					return i + 1;
				},
				dots:true,
				appendDots:$('.first-count'),				
			}
		},
	]
});




/*----------------------------------------
 SLICK CONTENT
 ----------------------------------------*/


$(window).on('load resize', function () {
	var width = $(document).width()
	if (width >= 640) {
		if ($('.content-list').hasClass('slick-initialized')) {
			$('.content-list').slick('unslick')
		}
	}
	else {
		$('.content-list').not('.slick-initialized').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true
		})
	}
})


/*----------------------------------------
	 MOBILE MENU
	 ----------------------------------------*/

$(".btn-menu").on("click", function () {
	$(".btn-menu").toggleClass("close-menu");
	$(".menu").toggleClass("menu_open");
	$("body").toggleClass("menu_open");
});

$('.close-menu').on('click', function () {
	$('.menu').removeClass('menu_open');
	$("body").toggleClass("menu_open");
});


/*----------------------------------------
	 FORM
	 ----------------------------------------*/

$(".btn-message").on("click", function () {
	$(".modal-form-message").toggleClass("form-open");
	$("body").toggleClass("menu_open");
});

$('.close-form').on('click', function () {
	$(".modal-form-message").removeClass("form-open");
	$("body").removeClass("menu_open");
	$(".modal-form-information").removeClass("form-open");
	$("body").removeClass("menu_open");
});

$(".btn-information").on("click", function () {
	$(".modal-form-information").toggleClass("form-open");
	$("body").toggleClass("menu_open");
});










/* form validation*/





	/*----------------------------------------
	 FOR SVG SPRITE
	 ----------------------------------------*/

	// svg4everybody({});

	// var click = document.querySelector('.click-me');

	// click.addEventListener('click', function () {
	//   alert('ok');
	// });




